/*
Realice los 10 ejercicios en el lenguaje Java, cada uno
de ellos estará en una método independiente
integrado en un solo programa principal
almacene su código fuente en un repositorio de control de versiones
en un documento doc, capture una imagen de cada uno de los ejercicios 
en su programa en ejecución
y escriba la ruta de su repositorio.  

Los ejercicios se encuentran en el material expuesto por el instructor.
 */
package com.mycompany.actividad1.Solis;

import java.util.Scanner;
public class Arreglos {
    private static class Act1{
           public Act1(){
               System.out.println("EJERCICIO 1");
               String[] compañeros = {"Edgar","Martin","Armando","Orlando","Phyton"};
               int [] edades = {30, 20, 20, 20, 19};
               int iPoint = 0;
               System.out.println(compañeros[iPoint]);//Edgar y su edad de 30 años
               System.out.println(edades[iPoint]);
               //******************************************
               System.out.println(compañeros[++iPoint]); //Martin y su edad de 20 años
               System.out.println(edades[iPoint]);
               //******************************************
               System.out.println(compañeros[++iPoint]);   //Armando y su edad de 20 años
               System.out.println(edades[iPoint]);
               //******************************************
               System.out.println(compañeros[++iPoint]);   //Orlando y su edad de 20 años
               System.out.println(edades[iPoint]);
               //******************************************
               System.out.println(compañeros[++iPoint]);  //Phyton y su edad de 19 años
               System.out.println(edades[iPoint]);
               System.out.println("........................");
           }
    }
      private static class Act2{
          public Act2(){
              System.out.println("EJERCICIO 2");
    String[] amigos = {"ADALBERTO", "NARDIN", "EDUARDO", "ROGER", "KAROL"};
        int[] edad = {22, 24, 23, 21, 22};
        int iPoint = 0;
        System.out.println(amigos[iPoint]);//Adalberto tiene 22
        System.out.println(edad[iPoint]);
        //*********************************
        System.out.println(amigos[++iPoint]); //Nardin tiene 24
        System.out.println(edad[iPoint]);
        //*********************************
        System.out.println(amigos[++iPoint]);//Eduardo tiene 23
        System.out.println(edad[iPoint]);
        //*********************************
        System.out.println(amigos[++iPoint]); //Roger tiene 21
        System.out.println(edad[iPoint]);
        //**********************************
        System.out.println(amigos[++iPoint]); //Karol tiene 22
        System.out.println(edad[iPoint]);
        System.out.println("................................");
          }
      }
      
            private static class Act3{
                public Act3(){
                 System.out.println("EJERCICIO 3");
                      double[] edaños = {18, 70, 50, 21 ,30};
                      double suma= 0;
                      double promediordaños;
                      double Totaledaños = 0; 
                      
                      for(double i: edaños){
                          suma += i; }
                      for(int i = 0; i < edaños.length; i++) {
            Totaledaños += edaños[i];
        }
                      promediordaños = Totaledaños / edaños.length;
                            System.out.println("La suma es: " + suma);
                            System.out.println("El promedio de las edades es: " + promediordaños + "años");
                            System.out.println("....................................");
                    
                    
                    
                    
                }
            }
       private static class Act4{
           public Act4(){
               System.out.println("EJERCICIO 4");
                 double[] datos = {1,2,3,4,5,6,7,8,9,10};
                 double suma = 0;
                 double promedio;
        
                for(double i: datos){
                suma += i;}
    
                           promedio = suma / datos.length;
                           System.out.println("La suma es: " + suma);
                           System.out.println("El promedio es : " + promedio);
                           System.out.println("...................................");
               
           }
       }
      //en esta actividad necesite ayuda de un compañero ya que se hace dificil este ejercicio
private static class Act5{
    public Act5(){
        Scanner sc = new Scanner(System.in);
        int Dat;
        System.out.println("EJERCICIO 5");
        System.out.println("Ingrese un número del 1 al 7");
        System.out.println("Nota:Considere que el dia Domingo comienza con el 1");
        Dat = sc.nextInt();
        String[] día = {"Domingo", "Lunes", "Martes", 
                           "Miércoles", "Jueves", "Viernes",
                           "Sábado"};
        if(Dat < 8 && Dat > 0){
            System.out.println(día[Dat- 1]);
        }else{
            System.out.println("El número " + Dat + " esta fuera del rango");
        }
        System.out.println("........................................");
   
        
    }
}       //aqui colabore en ideas con un compañero para la realizacion de la actividad
         private static class Act6{
             public Act6(){
                 System.out.println("EJERCICIO 6");
        
        String STRW [][] = new String [4][4];
        STRW [0][0]= "Luke Skywalker";
        STRW [0][1]= "R2-D2";
        STRW [0][2]= "C-3PO";
        STRW [0][3]= "Darth Vader";
        STRW [1][0]= "Leia Organa";
        STRW [1][1]= "Owen Lars";
        STRW [1][2]= "Beru Whitesun Lars";
        STRW [1][3]= "R5-D4";
        STRW [2][0]= "Biggs Darklighter";
        STRW [2][1]= "Obi-Wan Kenobi";
        STRW [2][2]= "Yoda";
        STRW [2][3]= "Jek Tono Porkins";
        STRW [3][0]= "Jabba Desilijic Tiure";
        STRW [3][1]= "Han Solo";
        STRW [3][2]= "Chewbacca";
        STRW [3][3]= "Anakin Skywalker";
        
        System.out.println("\nPersonajes de Star Wars (ciclo for simple): ");
        for(int i=0;i<4;i++){
        System.out.println(java.util.Arrays.toString(STRW[i]));
        }
        
        System.out.println("\nPersonajes de Star Wars (ciclo for each): ");
        for(String[] i:STRW){
        System.out.println(java.util.Arrays.toString(i));}
        
        
        System.out.println("...................................................");
             }
         }
       private static class Act7{
           public Act7(){
                  Scanner sc = new Scanner(System.in);
        System.out.println("EJERCICIO 7");
   
        String[] código = {"001-Calkini", "002-Campeche", "003-Carmen", 
                           "004-Champoton", "005-Hecelchakan", "006-Hopelchen",
                           "007-Palizada", "008-Tenabo", "009-Escarcega",
                           "010-Calakmul", "011-Candelaria", "012-Seybaplaya"};
        
        String[] cabeceras = {"Calkini", "San Francisco de Campeche", "Ciudad del Carmen",
                              "Champoton", "Hecelchakan", "Hopelchen", "Palizada",
                              "Tenabo", "Escarcega", "Xpujil", "Candelaria",
                              "Seybaplaya"};
        
        int[] habitantes = {52890, 259005, 221094, 83021, 28306, 37777,
                            8352, 10665, 54184, 26882, 41194, 15420};
        //Inicia parte uno
        for(int i = 0; i < 12; i++){
            System.out.println(código[i] + "   " + cabeceras[i] + "   " + habitantes[i]);}
        int num;
        int num2;
        System.out.println("");
        System.out.println("Ingrese un número del 1 al 12");
        num = sc.nextInt();
        
        
        if(num < 13 && num > 0){
            System.out.println("Resultado: "+código[num - 1] + "|" + cabeceras[num -1] + "|" + habitantes[num -1]);
        }else{
            System.out.println("El número " + num + "esta fuera del rango");
        }
        System.out.println("");
        System.out.println("Ingrese un número del 1 al 12");
        num2 = sc.nextInt();
        if(num2 > 0 && num2 < 13){
            switch (num2){
                case 1:
                    //desplegar posterior
                     System.out.println("Resultado: "+código[num2] + "|" + cabeceras[num2 -1] + "|" + habitantes[num2 -1]);
                     break;
                case 12:
                    //desplegar el anterior
                    num2--;
                    System.out.println("Resultado: "+código[num2 - 1] + "|" + cabeceras[num2 -1] + "|" + habitantes[num2 -1]);
                    break;
                default:
                    //desplegar el anterior y el posterior
                    num2--;
                    System.out.println("Resultado: "+código[num2 - 1] + "|" + cabeceras[num2 -1] + "|" + habitantes[num2 -1]);
                    num2++;
                     System.out.println("Resultado: "+código[num2] + "|" + cabeceras[num2 -1] + "|" + habitantes[num2 -1]);
                     break;
            }
           
        }else{
            System.out.println("El número " + num2 + "esta fuera del rango");
        }
        System.out.println("................................................");
        
           }
       }

     private static class Act8{
         public Act8(){
             
             System.out.println("EJERCICIO 8");
             
        int[] jos = new int[]{56,55,98,23,64};
        System.out.println("El primer elemento del array es: "+jos[0]);
        System.out.println("El último elemento del array es: "+jos[jos.length-1]);

        System.out.println(".........................................");
         }
     }
     public static class Act9{
         public Act9(){
             Scanner sc = new Scanner(System.in);
        int TRE;
        System.out.println("EJERCICIO 9");
        System.out.println("Ingrese un número del 1 al 12");
        
        TRE = sc.nextInt();
        String[] mes = {"Enero", "Febrero", "Marzo","Abril", "Mayo", "Junio",
             "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
        if(TRE < 13 && TRE > 0){
            System.out.println(mes[TRE - 1]);
        }else{
            System.out.println("El mes " + TRE + " no existe");
        }

        System.out.println("..............................................");
         }
     }
         private static class Act10{
        
        public static void LeerArray(int[] NombArreglo){
       String var = "";                           //declaro variable para concatenar
       
        for(int i=0;i<NombArreglo.length;i++){     //ciclo para leer los arreglos                
           var = var + NombArreglo[i];
               
        }
        
       if(var.indexOf("7")== -1){                 //metodo para valorar si esta el 7
             System.out.println("Aquí no se baila Mambo !!!"); 
                
            }else{
              
                System.out.println("Maaaambo");
             }    
    }
    public Act10(){
    System.out.println("Decimo ejercicio");
      int[] Arr1 = {1,2,3,4,5,6,7};            //Arreglo 1 Maaaambo            
         int[] Arr2 = {8,6,33,100};               //Arreglo 2 Aquí no se baila Mambo !!!
         int[] Arr3 = {2,55,37,91,65};            //Arreglo 3 Maaaambo
                
         System.out.println("Arreglo 1");           
         LeerArray(Arr1);                     //Utiliza para verficar si cuenta con el 7
         
         System.out.println("\nArreglo 2");
         LeerArray(Arr2);                     //Utiliza para verficar si cuenta con el 7
         
         System.out.println("\nArreglo 3");
         LeerArray(Arr3);

    }
     }
    public static void main(String[] args) {
        
        //todo codigo logico de aplicacion aqui xd
       Scanner sc = new Scanner(System.in);
        System.out.println("INICIANDO EJECUCIONES DE EJERCICIOS........");
        System.out.println("APROVADO..........................");
        System.out.println("SOLIS MAY JOSUE ISRAEL ");
          Act1 p1 = new Act1();
          Act2 p2 = new Act2();
          Act3 p3 = new Act3();
          Act4 p4 = new Act4();
          Act5 p5 = new Act5();
          Act6 p6 = new Act6();
          Act7 p7 = new Act7();
          Act8 p8 = new Act8();
          Act9 p9 = new Act9();
          Act10 p10 = new Act10();
         System.out.println("FIN DE LA EJECUCION DE TAREAS");
    }
    
}
